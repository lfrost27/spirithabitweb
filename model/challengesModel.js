import * as firebase from "firebase";

import { database } from '../api/firebase'; 

class ChallengesModel {

  static getUserChallenges() {
     return database.ref('/challenges').once('value').then(function(snapshot) {
     	 	const challenges = snapshot.val(); 
        return challenges;
	  });
  }

  static getChallengeDetail(id) { 
    	return database.ref('/challenges/' + id).once('value').then(function(snapshot) {
     	 	const challenge = snapshot.val();
        return challenge;
	  });
  }

  static createUpdateChallenge(challenges, habit) {
    return new Promise((resolve) => {
        let savedChallenges = {}; 
        Object.keys(challenges).map(function(key){

            let challengeKey = database
              .ref(`/challenges/${habit.key}`) //TODO
                .push(challenges[key]).key;

            let challengeHabitKey = database
              .ref(`/habits/${habit.key}/challenges/${challengeKey}`) //TODO
                .set(challenges[key]);

                savedChallenges[challengeKey] = challenges[key]; 
        });
        resolve(savedChallenges, habit);
    });
  }

  static updateChallenge(challenge) {
    return new Promise((resolve) => {
      let newRef = database.ref('/challenges/')
        .push(challenge);
      resolve(challenge);
    });
  }
}
export default ChallengesModel;
