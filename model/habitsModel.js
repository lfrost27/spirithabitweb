import * as firebase from "firebase";

import { database } from '../api/firebase'; 

class HabitsModel {

  static getUserHabits() {
     return database.ref('/habits').once('value').then(function(snapshot) {
     	 	const habits = snapshot.val(); 
        return habits;
	  });
  }

  static getHabitDetail(id) { 
    	return database.ref('/habits/' + id).once('value').then(function(snapshot) {
     	 	const habit = snapshot.val();
        return habit;
	  });
  }

  static createUpdateHabit(habit, tags) {
    var tagOject = {}; 

    return new Promise((resolve) => {
     let newRefHabitKey = ""; 

      if (habit.key === "") 
      {
           newRefHabitKey = database
            .ref('/habits/')
            .push(habit).key;
      } 
      else 
      {
           newRefHabitKey = habit.key; 
             database.ref(`/habits/${habit.key}`)
            .set(habit);
      }

      tagOject[newRefHabitKey] = true; 

      tags.map(function(key, tag) { 

        let newRefTags = database.ref(`/habitTags/${key.value}`)
          .push(tagOject);

          let habitOject = {}; 

          habitOject[key.value] = true; 


        let newTagHabitRef =  database.ref(`/habits/${newRefHabitKey}/tags/`)
          .push(habitOject);
    
      });

      habit.key = newRefHabitKey; 
      resolve(habit);
    });
  }

  static updateHabit(habit) {
    return new Promise((resolve) => {
      let newRef = database.ref('/habits/')
        .push(habit);
      resolve();
    });
  }
}
export default HabitsModel;
