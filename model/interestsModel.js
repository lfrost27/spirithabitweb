import * as firebase from "firebase";
import { database } from '../api/firebase'; 

class InterestsModel {

    static getInterests() {
      let interests = {}; 

       return database.ref('/tags').once('value').then(function(snapshot) {
          const interests = snapshot.val();
          return interests;
      });
    }

  static createUserInterests(id) { 
         return database.ref('/userHabit/' + id).once('value').then(function(snapshot) {
            const habits = snapshot.val(); 
            return habits;
      }); 
  }
}

export default InterestsModel;
