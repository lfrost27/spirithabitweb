import * as firebase from "firebase";

import { database } from '../api/firebase'; 

class UserModel {


  static getUserDetail(id) { 
    	return database.ref('/users/' + id).once('value')
        .then(function(snapshot) {
     	 	const user = snapshot.val();
        return user;
	  });
  }

  static createUpdateUser(user) {
    var tagOject = {}; 

    return new Promise((resolve) => {
      let userKey = database
        .ref('/users/')
          .push(user).key;

      resolve(user);
    });
  }

  static updateHabit(user) {
    return new Promise((resolve) => {
      let userKey = database.ref('/users/')
        .push(user);
      resolve();
    });
  }
}
export default UserModel;
