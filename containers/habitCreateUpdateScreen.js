import React, { input } from "react";
import { Provider, connect } from "react-redux";
import ReactDOM from 'react-dom';
import { Route, Link } from 'react-router-dom';
import { createUpdateHabit } from "../actions/habitsActions";
import { createUpdateChallenge } from "../actions/challengeActions";
import Select from 'react-select';
import ms from 'ms';
import { getAvailableInterests } from "../actions/interestsActions"

class HabitUpdateScreen extends React.Component {

  /*Convert array to helper*/
  constructor(props) {
    super(props);
    this.props = props; 
    this.state = {};
    this.state.habit = {
      key: '', 
      title: '',
      description: '',
      days: '', 
      startDate: '', 
      endDate: '', 
      shortTitle: '', 
      longTitle: '',
      selectedOption: null, 
      leader: {}, 
      isStateUpdating: false,
      isLoading: false
    };

    this.handleChange = this.handleChange.bind(this);
    this.selectChange = this.selectChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  componentDidMount() {
      this.props.onGetAvailalbeInterests().payload.then(interests => {

          let optionsArray = Object.keys(interests).map(function(key){
            return { "value" : interests[key].title.toLowerCase(),  "label": interests[key].title }; //, "label": interests[key].title
          });

          this.setState({ "options": optionsArray });
      });  
  }

  selectChange(selectedOption) {
    this.setState({selectedOption});
  }

  handleChange(event) {
    event.preventDefault();        

    var habit = this.state.habit; 

    if (event.target.id ==="title")
        habit.title = event.target.value;
    else if (event.target.id ==="description")
        habit.description = event.target.value;
     else if (event.target.id ==="days")
        habit.days = event.target.value;
     else if (event.target.id ==="startDate")
        habit.startDate = event.target.value;
     else if (event.target.id ==="endDate")
        habit.endDate = event.target.value;
      else if (event.target.id ==="shortTitle")
        habit.shortTitle = event.target.value;
      else if (event.target.id ==="longTitle")
        habit.longTitle = event.target.value;
      else if (event.target.id ==="leader")
        habit.leader = event.target.value;

        this.setState({ habit: habit });
  }

  initializeChallenges() { // 1) TODO::::::: This may be fine

    let challenges = this.state.challenges || {}; 
    let days = this.state.habit.days; 

    if(Object.entries(challenges).length === 0) {
      let challenge = {}; 
      for(let period = 0; period <= days - 1; period++)
        challenges[period] = challenge; 
    //  this.setState({ challenges: challenges });
    } else {
      challenges = this.state.challenges; 
    }

    return challenges; 
  }

  buildSubmittedChallenges(days) {

    var $doc = document; 

    var challenges = {}; 

    //Get key from parent container. Make sure it is populated 
    for(let period = 0; period <= days - 1; period++) {
     
      let challenge = {};  

      if ($doc.getElementById("chInputKey-" + period) != null)
        challenge.key = $doc.getElementById("chInputKey-" + period).value;

      if ($doc.getElementById("chShortTitle-" + period) != null)
       challenge.shortTitle = $doc.getElementById("chShortTitle-" + period).value; 
       
      if ($doc.getElementById("chLongTitle-" + period) != null)
       challenge.longTitle = $doc.getElementById("chLongTitle-" + period).value; 

      if ($doc.getElementById("chDescription-" + period) != null)
        challenge.description = $doc.getElementById("chDescription-" + period).value; 
      
      challenges[(challenge.key != "") ? challenge.key : period]  = challenge; 
        
    }

    return challenges;
  }

  handleSubmit(event) {
    event.preventDefault();        

    let habit = {}; 
    let tags = {};

    habit.shortTitle = this.state.habit.shortTitle; 
    habit.longTitle = this.state.habit.longTitle; 
    habit.description = this.state.habit.description; 
    habit.startDate = this.state.habit.startDate; 
    habit.endDate = this.state.habit.endDate; 
    habit.days = this.state.habit.days;
    habit.key = this.state.habit.key

    tags = this.state.selectedOption;
    habit.status = "open";

    let challenges = this.buildSubmittedChallenges(Number(habit.days)); 
    
    this.props.onCreateHabitDetail(habit, tags).payload.then((habit, tags) => {
         //Callback - Return Habit and update challenge
         //What if saved again. Use the same Habit Id 
        this.setState({ habit: habit, isLoading: true });

        if (challenges != undefined) //should always be undefined
          this.props.onCreateChallenge(challenges, habit).payload
            .then((challenges, habit) => {
                this.setState({ challenges: challenges, isLoading: false });
            }); 
    });   
        
  }

  render() {

    //Update State to reload this shit 
    let habit = this.state.habit || {};
    let { options } = this.state || {};
    const { selectedOption } = this.state;

    if (this.state.isLoading) {
      return <p>Loading...</p>;
    }

    // TOEDO: Check for State Challenges first. Get ID
  //  let challenges = this.initializeChallenges(); // 2) TODO::::::: RETURN Challenges in object list if we have something there, or initialize objects
   // let state = this.state; 

      return (
        <div>
            <h2>Create a Habit</h2>
              <form onSubmit={this.handleSubmit}>
                  <input type="hidden" id="habitKey" value={this.state.key}></input>
                  <label>Leader3: <input id="leader" type="text" value={this.state.habit.leader} onChange={this.handleChange} /></label>
                  <label>Short Title: <input id="shortTitle" type="text" value={this.state.habit.shortTitle} onChange={this.handleChange} /></label>
                  <label>Long Title: <input id="longTitle" type="text" value={this.state.habit.longTitle} onChange={this.handleChange} /></label>
                  <label>Description: <input id="description" type="text" value={this.state.habit.description} onChange={this.handleChange} /></label>
                  <label># of Days: <input id="days" type="text" value={this.state.habit.days} onChange={this.handleChange} /></label>
                  <label>Start Date: <input id="startDate" type="text" value={this.state.habit.startDate} onChange={this.handleChange} /></label>
                  <label>End Date: <input id="endDate" type="text" value={this.state.habit.endDate} onChange={this.handleChange} /></label>
                  <label>Tag Options:<Select id="tagOptions" isMulti value={selectedOption} onChange={this.selectChange} options={options} /></label>
                  <button type="submit">Save Habit</button>
              </form>
              <div><ChallengesComponent challenges={this.initializeChallenges()} /></div>
          </div>     
        );
    }
  }

function mapStateToProps (state) {
  return {
    habit: state.habit, 
    challenges: state.challenges
  } 
}

function ChallengesComponent (props) {
 return (
    Object.keys(props.challenges).map((key, index) => { 
      let i = index; 
      let shortTitleId = 'chShortTitle-' + i;
      let longTitleId = 'chLongTitle-' + i;
      let descriptionId = 'chDescription-' + i;
      let periodId = 'chLblPeriod-' + i;
      let challengeKey = 'chInputKey-' + i;
       return  (
                <div key={["challengeContainer",,i]}>
                  <input type="hidden" key={["challengeKey",,i]} id={challengeKey} value={key}></input>
                  <label key={["lblPeriod",, i]} id={periodId}>Day {i + 1}</label>
                  <label key={["lblShortTitle",, i]}>Short Title: <input key={["shortTitle",, i]} id={shortTitleId} type="text" /></label>
                  <label key={["lblLongTitle",, i]}>Long Title: <input key={["shortTitle",, i]} id={longTitleId} type="text" /></label>
                  <label key={["lblDescription",, i]}>Description: <input key={["shortDescription",, i]} id={descriptionId} type="text" /></label>
                </div>
        )
      })
  );
}

function mapDispatchToProps (dispatch, ownProps) {  //ownProps
  return { 
    onCreateHabitDetail:(habit, tags) => dispatch(createUpdateHabit(habit, tags)),  //TODO: Pass in the Habit Detail. This is duplicate??
    onCreateChallenge:(challenges, habit) => dispatch(createUpdateChallenge(challenges, habit)),  //TODO: Pass in the Habit Detail. This is duplicate??
    onGetAvailalbeInterests:() => dispatch(getAvailableInterests())
  };
} 

export default connect(mapStateToProps, mapDispatchToProps)(HabitUpdateScreen);
