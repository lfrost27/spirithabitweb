import React from "react";
import { Provider, connect } from "react-redux";
import ReactDOM from 'react-dom';
import { Route, Link } from 'react-router-dom';
import { getHabitDetail } from "../actions/habitsActions";
import { getAvailableInterests } from "../actions/interestsActions"
import Select from 'react-select';
import ms from 'ms';


/* This screen is in the web app. Where a user defines interests */ 
class HabitsDetailScreen extends React.Component {

  constructor(props) {
    super(props);
    this.props = props;
  }

  componentDidMount() {

    let options; 
      this.props.onGetHabitDetail().payload.then(habit => {
         this.setState({ habit: habit });
      }); 

      this.props.onGetAvailableInterests().payload.then(interests => {
        this.setState({ options: interests });
      });           
  }

  updateHabits(event) {
    event.preventDefault();        
  }

  addInitialChallenges() {

  }

  render() {
    let { habit } = this.state || {};
    let { options } = this.state || {};
    console.log("options: ")
    console.log(options);

    //Check Habit.Challenges
    //Create Challenges for Each Day 

      return (
        <div>
          <div>
            <div>Habit Screen</div>
            {habit && habit != undefined ? ( 
              <div>
                <div>
                  <div>{habit.shortTitle}</div>
                  <div>{habit.longTitle}</div>
                  <div>{habit.days}</div>
                  <div>{habit.startDate}</div>
                  <div>{habit.description}</div>
                  <div>{habit.tags}</div>
                </div>
                <form onSubmit={this.updateHabits}>
                    <label>Leader2: <input id="leader" type="text" value={this.state.leader} onChange={this.handleChange} /></label>
                    <label>Short Title: <input id="shortTitle" type="text" value={this.state.shortTitle} onChange={this.handleChange} /></label>
                    <label>Long Title: <input id="longTitle" type="text" value={this.state.longTitle} onChange={this.handleChange} /></label>
                    <label>Description: <input id="description" type="text" value={this.state.description} onChange={this.handleChange} /></label>
                    <label>Tags: <input id="tags" type="text" value={this.state.tags} onChange={this.handleChange} /></label>
                    <button type="submit">Save Habit</button>
                    <button type="submit">Publish</button>
                </form>
              </div>
            ) : null}
             
                <div>
                  <h2>Initial Challenges</h2> 
                </div>
          </div>
        </div>);
    }
  }

function mapStateToProps (state) {
  return {
    habitData: state.habits,
    interestData: state.interests
  } 
}

function mapDispatchToProps (dispatch, ownProps) {  //ownProps
  var id = ownProps.match.params.id; 
  return { 
    onGetHabitDetail: () => dispatch(getHabitDetail(id)),  //TODO: Pass in the Habit Detail. This is duplicate??
    onGetAvailableInterests: () => dispatch(getAvailableInterests())
  };
} 

export default connect(mapStateToProps, mapDispatchToProps)(HabitsDetailScreen);
