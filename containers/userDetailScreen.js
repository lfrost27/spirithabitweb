import React from "react";
import { Provider, connect } from "react-redux";
import ReactDOM from 'react-dom';
import { Route, Link } from 'react-router-dom';
import { getUserDetail } from "../actions/userActions";
import { createUpdateUser } from "../actions/userActions";

class UserDetailScreen extends React.Component {

  constructor(props) {
    super(props);
    this.props = props;
    this.state = {
      firstName: '',
      lastName: '',
      avatar: '', 
      startDate: '', 
      endDate: '', 
      bio: '', 
      longTitle: '',
    }
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  componentDidMount() {
      this.props.onGetUserDetail().payload.then(user => {
         this.setState({ user: user });
      });          
  }

  handleSubmit(event) {
    event.preventDefault();        

    let user = {}; 

    user.firstName = this.state.firstName; 
    user.lastName = this.state.lastName; 
    user.avatar = this.state.avatar; 
    user.startDate = this.state.startDate; 
    user.endDate = this.state.endDate; 
    user.tagline = this.state.tagline;
    user.bio = this.state.bio;

    this.props.onCreateUserDetail(user).payload.then((user) => {
        this.setState({ user: user });
         //Callback - Return Habit and update challenge 
        /*   if (challenges != undefined)
          this.props.onCreateChallenge(challenges, habit).payload
            .then((challenges, habit) => {});  */
    });   
  }
  
  openSelected (index, user, navigate) {

  }

  addSelected (index, user, navigate) {
  
  }

  handleChange(event) {
    event.preventDefault();        

    if (event.target.id ==="firstName")
        this.setState({ firstName: event.target.value });
    else if (event.target.id ==="lastName")
        this.setState({ lastName: event.target.value });
     else if (event.target.id ==="avatar")
        this.setState({ avatar: event.target.value });
     else if (event.target.id ==="startDate")
        this.setState({ startDate: event.target.value });
     else if (event.target.id ==="endDate")
        this.setState({ endDate: event.target.value });
      else if (event.target.id ==="tagline")
        this.setState({ tagline: event.target.value });
      else if (event.target.id ==="bio")
        this.setState({ bio: event.target.value });
  }

  render() {
    let { user } = this.state || {};

      return (
        <div>
          <div>
            <h1>User</h1>
              <div>
              {user && user != undefined ? (
                <div>
                  <div>{user.avatar}</div>
                  <div>{user.username}</div>
                  <div>{user.firstName}</div>
                  <div>{user.lastName}</div>
                  <div>{user.level}</div>
                  <div>{user.startDate}</div>
                  <div>{user.enabled}</div>
                  <div>{user.bio}</div>
                  <div>{user.tagline}</div>
                </div>
                ) : null}
                <form onSubmit={this.handleSubmit}>
                  <label>Avatar <input id="avatar" type="text" value={this.state.avatar} onChange={this.handleChange} /></label>
                  <label>First Name: <input id="firstName" type="text" value={this.state.firstName} onChange={this.handleChange} /></label>
                  <label>Last Name: <input id="lastName" type="text" value={this.state.lastName} onChange={this.handleChange} /></label>
                  <label>Bio: <input id="bio" type="text" value={this.state.bio} onChange={this.handleChange} /></label>
                  <label>Tagline: <input id="tagline" type="text" value={this.state.tagline} onChange={this.handleChange} /></label>
                  <button type="submit">Save Habit</button>
                  <button type="submit">Publish</button>
                </form>
            </div>
        </div>
      </div>);
    }
  }

function mapStateToProps (state) {
  return {
    userData: state.user
  } 
}

function mapDispatchToProps (dispatch, ownProps) {  //
  var id = ownProps.match.params.id; 
  return { 
    onGetUserDetail: () => dispatch(getUserDetail(id)),  
    onCreateUserDetail: (user) => dispatch(createUpdateUser(user)), 
  };
} 

export default connect(mapStateToProps, mapDispatchToProps)(UserDetailScreen);

