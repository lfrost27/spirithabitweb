import React from "react";
import { Provider, connect } from "react-redux";
import ReactDOM from 'react-dom';
import { Route, Link } from 'react-router-dom';
import { getHabitDetail, getUserHabits, setSelectedHabits } from "../actions/habitsActions";

class HabitsListScreen extends React.Component {

  constructor(props) {
    super(props);
    this.props = props; 
  }

  componentDidMount() {
      this.props.onGetUserHabits().payload.then(habits => {
         this.setState({ habits: habits });
      });          
  }
  
  openSelected (index, habit, navigate) {
 
  }

  addSelected (index, habit, navigate) {
      habit.isSelected = (habit.isSelected === true) ? false : true; 
      let tempHabits = this.state.selectedHabits || {};
      let Habit = {}; 

      Habit.title = habit.title;

      let tempChallenges = []; 
      let Challenge = {}; 

      console.log (habit);

      if (habit.isSelected) {
        tempHabits[index] = Habit; 
      } else { 
        if (tempHabits && tempHabits[index]) //Array
            delete tempHabits[index]
      } 

    this.setState({ "selectedHabits": tempHabits, "habits": this.state.habits });
    store.dispatch(setSelectedHabits(this.state));

    return; 
  }

  render() {
    let { habits } = this.state || {};
   // const { navigate } = this.props.navigation;
      return (
        <div>
          <div>
            <div>Habits List Screen</div>
            <Link to={`/habit/create`}>Create New Habit</Link>
            <Link to={`/user/create`}>Create New User</Link>
            {habits && habits != undefined ? (
              <ul>
                {Object.keys(habits).map((key, index) => {
                  return <li key={index}>
                    <Link to={`/habit/${key}`}>{habits[key].shortTitle}</Link>
                  </li>
                })}
              </ul>
            ) : null}
          </div>
        </div>);
    }
  }

function mapStateToProps (state) {
  return {
    habitData: state.habits
  } 
}

function mapDispatchToProps (dispatch, ownProps){
  return { 
    onGetUserHabits: () => dispatch(getUserHabits()),
  };
} 

export default connect(mapStateToProps, mapDispatchToProps)(HabitsListScreen);
