import React from "react";
import { Provider, connect } from "react-redux";
import ReactDOM from 'react-dom';
import { Route, Link } from 'react-router-dom';
import { getHabitDetail } from "../actions/habitsActions";

class ChallengeListContainer extends React.Component {

  constructor(props) {
    super(props);
    this.props = props; 
  }

  componentDidMount() {
      this.props.onGetHabitDetail().payload.then(habit => {
         this.setState({ habit: habit });
      });          
  }
  
  openSelected (index, habit, navigate) {

  }

  addSelected (index, habit, navigate) {
  
  }

  render() {
    let { habit } = this.state || {};

      return (
        <div>
          <div>
            <div>Habit Screen</div>
            {habit && habit != undefined ? (
              <div>
                <div>{habit.shortTitle}</div>
                <div>{habit.longTitle}</div>
                <div>{habit.days}</div>
                <div>{habit.startDate}</div>
              </div>
            ) : null}
          </div>
        </div>);
    }
  }

function mapStateToProps (state) {
  return {
    habitData: state.habits
  } 
}

function mapDispatchToProps (dispatch, ownProps) {  //ownProps
  var id = ownProps.match.params.id; 
  return { 
    onGetHabitDetail: () => dispatch(getHabitDetail(id)),  //TODO: Pass in the Habit Detail. This is duplicate??
  };
} 

export default connect(mapStateToProps, mapDispatchToProps)(HabitsDetailScreen);
