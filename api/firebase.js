import firebase from 'firebase';

const config = {
  apiKey: "AIzaSyB7DFzxYwAOna4yITdBmaG9C7YlWfIoOuU",
  authDomain: "spirithabit-a303c.firebaseio.com",
  databaseURL: "https://spirithabit-a303c.firebaseio.com/",
  storageBucket: "gs://spirithabit-a303c.appspot.com/",
};

firebase.initializeApp(config);

export const database = firebase.database();
export const authentication = firebase.auth();
export const auth = firebase.auth;

