import ChallengesModel from '../model/challengesModel';


export function getUserChallenges() {
	return  {
		type: "challenges/getUserChallenges",
    	payload:  ChallengesModel.getUserChallenges()
    };
}

export function getChallengeDetail(id) {
	return  {
		type: "challenges/getChallengeDetail",
    	payload:  ChallengesModel.getChallengeDetail(id)
    };
}

export function createUpdateChallenge(challenges, habit) {
	return  {
		type: "challenges/createUpdateChallenge",
    	payload:  ChallengesModel.createUpdateChallenge(challenges, habit)
    };
}

export function setSelectedChallenges(challenge) {
	return  {
		type: "interests/setSelectedChallenges",
    	payload:  selectedChallenges
    };
}
