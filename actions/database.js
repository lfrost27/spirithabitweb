import firebase from 'firebase';

  var config = {
    apiKey: "AIzaSyC8srD7KPF-2Ss1r1HWJqvRyRTzuTYrmH4",
    authDomain: "meeting-app-a7121.firebaseapp.com",
    databaseURL: "https://meeting-app-a7121.firebaseio.com/",
    projectId: "meeting-app-a7121",
    storageBucket: "meeting-app-a7121.appspot.com",
    messagingSenderId: "854367158553"
  };

firebase.initializeApp(config);
const database = firebase.database();

export default database;
