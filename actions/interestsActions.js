import InterestsModel from '../model/interestsModel';

export function getAvailableInterests() {
	return  {
		type: "interests/getAvailableInterests",
    	payload:  InterestsModel.getInterests()
    };
}

export function setSelectedInterest(selectedInterests) {
	return  {
		type: "interests/setSelectedInterests",
    	payload:  selectedInterests
    };
}

export function createUserInterests(selectedInterests) {
	return  {
		type: "interests/createSelectedInterests",
    	payload:  selectedInterests
    };
}



