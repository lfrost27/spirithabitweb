import HabitsModel from '../model/habitsModel';


export function getUserHabits() {
	return  {
		type: "habits/getUserHabits",
    	payload:  HabitsModel.getUserHabits()
    };
}

export function getHabitDetail(id) {
	return  {
		type: "habits/getHabitDetail",
    	payload:  HabitsModel.getHabitDetail(id)
    };
}

export function createUpdateHabit(habit, tags) {
	return  {
		type: "habits/createUpdateHabit",
    	payload:  HabitsModel.createUpdateHabit(habit, tags)
    };
}

export function setSelectedHabits(selectedHabits) {
	return  {
		type: "interests/setSelectedHabits",
    	payload:  selectedHabits
    };
}
