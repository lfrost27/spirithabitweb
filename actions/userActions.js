import UserModel from '../model/userModel';

export function getUserDetail(id) {
	return  {
		type: "users/getUserDetail",
    	payload: UserModel.getUserDetail(id)
    };
}

export function getUserActivities() {
	return  {
		type: "users/getUserActivities",
    	payload: UserModel.getUserActivities()
    };
}
export function createUpdateUser(user) {
	return  {
		type: "users/createUpdateUser",
    	payload: UserModel.createUpdateUser(user)
    };
}
