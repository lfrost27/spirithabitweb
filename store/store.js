import { createStore, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import createLogger from "redux-logger";
import rootReducerWithRouter from '../reducers/rootReducer';

const logger = createLogger();

/*export default createStoreWithMiddleware = createStore(rootReducerWithRouter,
  {},
  applyMiddleware(thunk, logger)
);*/
const store = createStore(rootReducerWithRouter); 

export default store;

