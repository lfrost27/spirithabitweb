import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import HabitsListScreen from './containers/habitListScreen.js';
import HabitDetailScreen from './containers/habitDetailScreen.js';
import HabitUpdateScreen from './containers/habitCreateUpdateScreen.js';
import UserDetailScreen from './containers/userDetailScreen.js';

import store from './store/store';

import { connectRouter } from 'connected-react-router';
import { Switch, Router, Route, hashHistory } from 'react-router-dom';
import { syncHistoryWithStore, routerReducer } from 'react-router-redux';
import { createBrowserHistory } from 'history'; 

import './stylesheets/main.scss';

const history = syncHistoryWithStore(createBrowserHistory(), store);

const main = (
  <Provider store={store}>
   	<Router history={history}>
   	 	<div>
   	 		<Route exact path="/" component={HabitsListScreen}/>
	   	 	<Route path="/habit/:id" component={HabitDetailScreen}/>
	   	 	<Route path="/habit/create" component={HabitUpdateScreen}/> 
	   	 	<Route path="/user/create" component={UserDetailScreen}/>
	   	 </div>
	</Router> 
  </Provider>
);

ReactDOM.render(main, document.getElementById('container'));
 