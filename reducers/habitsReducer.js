export function habitsReducer(state={}, action) {
    
    switch (action.type) {

    case "habits/createUpdateHabit": {
      const newState = Object.assign({}, state, {
        inProgress: false,
        challenges: action.payload, 
        habit: action.payload, 
        success: 'Created Habit.'
      });

    	return newState
	}

    default:
    	return state; 
	}
}