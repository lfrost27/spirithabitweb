import { combineReducers } from 'redux';
import { routeReducer } from 'react-router-redux';
import { connectRouter } from 'connected-react-router';
import { habitsReducer } from './habitsReducer';
import { challengesReducer } from './challengesReducer';

import { syncHistoryWithStore, routerReducer } from 'react-router-redux';

const rootReducer = combineReducers({ 
  	routing: routerReducer,
  	habit: habitsReducer, 
  	challenges: challengesReducer
});

const rootReducerWithRouter = connectRouter(history)(rootReducer);

export default rootReducerWithRouter;
