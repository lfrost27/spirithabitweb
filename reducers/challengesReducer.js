export function challengesReducer(state={}, action) {
    
    switch (action.type) {

    case "challenges/createUpdateChallenge": {
      const newState = Object.assign({}, state, {
        inProgress: false,
        challenges: action.payload, 
        habit: action.payload, 
        success: 'Created Habit.'
      });

    	return newState
	}

    default:
    	return state; 
	}
}