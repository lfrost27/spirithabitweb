export function usersReducer(state={}, action) {
    
    switch (action.type) {

      case "users/getUsers": {
        const newState = Object.assign({}, state, {
          inProgress: false,
          users: action.payload, 
          success: 'Loading User.'
        });

      	return newState
      }

      case "users/getUserDetail": {
        const newState = Object.assign({}, state, {
          inProgress: false,
          users: action.payload, 
          success: 'Loading User Details.'
        });
        return newState
      }

      case "users/getUserDetailByAuth": {
        const newState = Object.assign({}, state, {
          inProgress: false,
          users: action.payload, 
          success: 'Loading User Details.'
        });
        return newState
      }

    default:
    	return state; 
  }
  
}