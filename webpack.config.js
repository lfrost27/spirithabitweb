module.exports = {
  devtool: 'inline-source-map',
  entry: __dirname + '/app.jsx',
  output: {
    filename: 'bundle.js'
  },
  historyApiFallback: true,
  module : {
    loaders : [
      {
        test : /\.jsx?/,
        loader : 'babel',
        exclude: /node_modules/
      },
      {
        test: /\.scss$/,
        loaders: ['style', 'css', 'sass']
      }
    ]
  }
}
